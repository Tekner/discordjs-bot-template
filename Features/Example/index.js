const client = require('client');
const phrases = require('./phrases.json');

const triggerPhrase = 'discordbot';

function GetWordRegex(word)
{
	var regexWord = "";
	for(var i = 0; i < word.length; i++) {
		regexWord += "(?=" + word[i] + ")([" + word[i] + "\\-\\+\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\[\\]\\{\\}\\|\\\\\\:\\;\\\"\\<\\>\\,\\.\\/\\?\\d]+)";
	}
	return new RegExp('[^a-z]' + regexWord + '[^a-z]', 'g');
}

function MessageContainsWord(content, word)
{
	return GetWordRegex(word).test(' ' + content + ' ');
}

function CheckMessage(message)
{
	var content = message.content.toLowerCase();
	if (message.author.bot || !MessageContainsWord(content, triggerPhrase)) {
		return;
	}

	for(var phrase in phrases) {
		var response = phrases[phrase];
		if (MessageContainsWord(content, phrase)) {
			message.channel.send(response);
			break;
		}
	}
}

function CleanUp()
{
	console.log("Cleaning up Example Feature");
}


client.on('message', CheckMessage);
client.on('cleanup', CleanUp);