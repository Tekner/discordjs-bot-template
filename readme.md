# Discord.js Bot Template

## Introduction

This is a template to create your own Discord.js bot really easily.  
It adds a little infrastructure to get you going faster.  
This includes:
* Automatic cleanup and logout
* Automatic feature loading to allow easy modularization of bot logic
* Local client module for easy referencing of Discord.js client object
* .env variable loading for easy customization of environment data

## Getting Started

After cloning this repository, make sure to rename `.env.template` to `.env` and fill in the **BOT_TOKEN** variable.

Make sure you have nodejs and npm installed on your system, and then run `npm install`.

After you have installed the dependencies, simply run `npm start` to launch your bot!

## Additional Information

An example Feature has been provided for you.  
To create your own Feature, simply create a new folder in the `Features` directory and add an `index.js` file to it. This file will be auto-loaded when the bot starts and all of your Discord.js logic will be put here. You can put anything you want in your Feature folder that will get used by your Feature logic.  
If you want to disable a feature without deleting the code, you can append ` (Disabled)` to the Feature folder name (note the space.) E.g. `Example (Disabled)`. When disabled, the feature will simply be skipped and not autoloaded, but is otherwise still accessible to the rest of your bot.  
Subscribe to the `cleanup` event to run any cleanup code before the process terminates, such as closing database or socket connections. This is also shown in the example provided.  
You will find an `EXIT_KEY` in the `.env.template` file. This is provided as quality of life to provide an easy way to terminate the bot process safely (it will perform all coded cleanup logic.) By default the exit key is "Q".

## The Provided Example

The provided feature is a simple example which has the bot respond to some preset phrases.  
By default, the triggering keyword is "discordbot". As long as a message contains that word and a phrase described in `/Features/Example/phrases.json`, the bot will respond with the associated response also in that file.  
For example, if you type "hi discordbot", the bot will respond back with "Hi there!".  
Or if you type "discordbot, look what you did!", the bot will respond back with ">.<*..."