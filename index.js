process.setMaxListeners(0);

function Log(message)
{
	var d = new Date();
	d.setTime( d.getTime() - d.getTimezoneOffset()*60*1000 );
	console.log(`[${d.toISOString()}] ${message}`);
}

function CleanUp()
{
	try {
		const client = require('client');
		client.emit('cleanup');
	} catch(err) { }
}

try
{
	//Discord API Setup
	const glob = require('glob');
	const path = require('path');
	const client = require('client');

	client.on('ready', () => {
		Log(`Logged in as ${client.user.tag}!`);

		//Load Features
		glob.sync('./Features/*/').forEach(function(dir) {
			var basename = path.basename(dir);
			if (basename.length < 11 || basename.substr(basename.length-11) !== ' (Disabled)') {
				require(path.resolve(dir) + '/index.js');
				Log(basename + ' Feature Loaded!');
			}
		});

		//After all features' cleanup events have ran, log out.
		client.on('cleanup', () => {
			client.destroy();
			Log(`${process.env.BOT_NAME} Fully Shut Down`);
		});

		//Attempt to clean up when the process is being terminated
		process.on('exit', CleanUp.bind());
		process.on('SIGINT', CleanUp.bind());
		process.on('SIGUSR1', CleanUp.bind());
		process.on('SIGUSR2', CleanUp.bind());

		Log(`${process.env.BOT_NAME} Fully Initialized`);
	});

	const readline = require('readline');

	readline.emitKeypressEvents(process.stdin);
	process.stdin.setRawMode(true);
	process.stdin.on('keypress', (_, key) => {
		if (key.name === process.env.EXIT_KEY) {
			process.exit();
		}
	});
} catch(err) {
	Log("ERROR: " + err);
}